using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    Quizz quizz;
    Finish finish;
    
    void Awake() 
    {
        quizz = FindObjectOfType<Quizz>();
        finish = FindObjectOfType<Finish>();
    }

    void Start()
    {
        quizz.gameObject.SetActive(true);
        finish.gameObject.SetActive(false);
    }

    void Update()
    {
        if(quizz.isComplete == true)
        {
            quizz.gameObject.SetActive(false);
            finish.gameObject.SetActive(true);
            finish.ShowFinalScore();
        }

    }

    public void OnReplayLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
